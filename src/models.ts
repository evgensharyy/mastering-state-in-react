export interface IUser {
    id: string,
    avatar: string,
    firstName: string,
    lastName: string,
    position: string
}

 