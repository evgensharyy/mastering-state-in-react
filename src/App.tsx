import React, { useEffect, useState } from 'react';
import Community from './component/Community'
import JoinOurProgram from './component/JoinOurProgram'

function App() {
  
  const [hidden, setHidden] = useState(false);

  function hideCommunity(){
    setHidden((prev) =>{
      return !prev
    })
  }

  return (
    <>
      <section className="app-community app-community--background-white">
        <div className="hide-section">
        <h2 className="app-title">Big Community of People Like You</h2>
        {hidden ? <button onClick={hideCommunity} className="hide-section_button--green">SHOW SECTION</button> : <button onClick={hideCommunity} className="hide-section_button--red">HIDE SECTION</button>}
          </div>
        <h3 className="app-subtitle">We’re proud of our products, and we’re <br></br> really excited when we get feedback from our users.</h3>
        {hidden ? null : <Community />}
      </section>
      <section className="app-join app-join--image-peak">
        <h2 className="app-title">Join Our Program</h2>
        <h3 className="app-subtitle">Sed do eiusmod tempor incididunt<br></br> ut labore et dolore magna aliqua.</h3>
        <JoinOurProgram />
      </section>
    </>
  );
}

export default App;
