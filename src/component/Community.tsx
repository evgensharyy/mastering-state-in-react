import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../App.css';
import Card from './Card'
import { IUser } from '../models'

function Community() {
    const [users, setUsers] = useState<IUser[]>([]);

    async function fetchUsers() {
        const response = await axios.get<IUser[]>('http://localhost:3000/community')
        setUsers(response.data)
    }

    useEffect(() => {
        fetchUsers()
    }, [])

    return (
        <div className="app-collection">
          {users.map(user => <Card user={user} key={user.id} />)}
        </div>
    );
}

export default Community;