import React from 'react';
import { IUser } from '../models'

interface UsersProps {
    user: IUser
}

function Card(props: UsersProps) {
    return (
        <>
        <div className="app-card">
            <img className="app-card__image" alt="worker photo" src={props.user.avatar}></img>
            <p className="app-card__name">{props.user.firstName}</p>
            <p className="app-card__job-title">{props.user.position}</p>
        </div>
        </>
    );
}

export default Card;