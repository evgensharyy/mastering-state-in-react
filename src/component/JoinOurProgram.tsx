import React, { useEffect, useState } from 'react';
import '../App.css';
import { Form, Field } from 'react-final-form';
import axios from 'axios';

type Value = undefined | string;

interface Values {
    email: string;
  }

const required = (value: Value) => (value ? undefined : 'Required')

function JoinOurProgram() {
    const [subscriber, setSubscriber] = useState(false);

    useEffect(() => {
        console.log('SUBSCRIBER:', subscriber);
    })

    const onSubmit = (values: Values) => {
        axios.post('http://localhost:3000/subscribe', {
            email: values.email,
          })
          .then(function (response) {
            if(response.status === 200){
                setSubscriber(true);
            }
            console.log(response);
          })
          .catch(function (error) {
            if(error.response.status === 422){
                window.alert(error.response.data.error)
            }
            console.log(error);
          });
    }

    function onUnsubscribe() {
        axios.post('http://localhost:3000/unsubscribe')
          .then(function (response) {
            if(response.status === 200){
                console.log('TEST');
                console.log(response);
                
                setSubscriber(false);
            }
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
    }


    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit, form, submitting, pristine, values }) => (
                <form className="app-join__form" onSubmit={handleSubmit}>
                    <Field name="email" validate={required}>
                        {({ input, meta }) => (
                            <div>
                                {subscriber ? null : <label>Email: </label>}
                                {subscriber ? null : <input className="app-join__input" {...input} type="text" placeholder="Email" />}
                                {meta.error && meta.touched && <span className="app-join__input--error">{meta.error}</span>}
                            </div>
                        )}
                    </Field>
                        {subscriber ? null : <button className="app-join__button app-join__button--subscribe" type="submit" disabled={submitting || pristine}>Subscribe</button>}
                        {subscriber ? <button className="app-join__button app-join__button--subscribe" onClick={onUnsubscribe}>Unsubscribe</button> : null}
                </form>
            )}
        />
    );
}

export default JoinOurProgram;